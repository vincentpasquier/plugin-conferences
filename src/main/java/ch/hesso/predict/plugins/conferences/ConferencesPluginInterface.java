/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.conferences;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.plugins.conferences.processors.ConferenceProcessor;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.annotations.CallableMethod;
import ch.hesso.websocket.plugins.PluginInterface;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.NERClassifierCombiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ConferencesPluginInterface extends PluginInterface {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( ConferencesPluginInterface.class );

	protected static final String NER;

	public static AbstractSequenceClassifier CLASSIFIER;

	static {
		String ner = System.getProperty ( "predict.plugins.conferences.ner" );
		if ( ner == null ) {
			NER = "";
			LOG.error ( "NER property not set (predict.plugins.conferences.ner). Please set it before starting." );
			System.exit ( -1 );
		} else {
			NER = ner;
		}
		try {
			CLASSIFIER = NERClassifierCombiner.loadClassifierFromPath ( NER );
		} catch ( FileNotFoundException e ) {
		}
	}

	@CallableMethod ( value = "Extract conferences", description = "Launches a conference website extraction from existing CFPs." )
	public void extractConferences () {
		ConferenceProcessor processor = new ConferenceProcessor ();
		processor.run ();
	}

	@Override
	public String name () {
		return "Conference website committee extraction";
	}

	@Override
	public String description () {
		return "Performs a conference website extraction aimed at committee extraction to provide with the recommendation system with meaningful data.";
	}

	@Override
	public Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources () {
		return new HashMap<> ();
	}

	@Override
	public Set<APIEntityVisitor> visitors () {
		return new HashSet<> ();
	}
}
