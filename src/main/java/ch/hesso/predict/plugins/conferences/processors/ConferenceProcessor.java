/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.conferences.processors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.CFPClient;
import ch.hesso.predict.client.resources.PeopleClient;
import ch.hesso.predict.client.resources.SessionClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.conferences.ConferenceUtils;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.Conference;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.WebPage;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ConferenceProcessor {

	private final PluginInterface.PluginStatus.Builder builder;

	private final Set<Cfp> allCfp = new HashSet<> ();

	private final Multimap<String, String> confIdMappingWebsiteRoot;

	private final Multimap<String, String> confIdMappingPCMemberPage;

	private final Multimap<String, String> pcNameMappingConfId;

	private final Multimap<String, String> pcIdMappingConfId;

	private int conferenceRootWebsite = 0;

	public ConferenceProcessor () {
		builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		builder.progress ( 1 ).totalProgress ( 6 );
		confIdMappingWebsiteRoot = HashMultimap.create ();
		confIdMappingPCMemberPage = HashMultimap.create ();
		pcNameMappingConfId = HashMultimap.create ();
		pcIdMappingConfId = HashMultimap.create ();
	}

	public void run () {

		// Get all CFP
		boolean success = getAllCFP ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract all CFP, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 );

		// For all CFP -> conferenceId
		// If exists -> confId -> website
		success = getAllConferencesWithCFP ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to filter Conferences with CFP, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 3 );

		// Get root website
		// If website has page
		// Add to list -> confId, website
		success = parseRootWebsites ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Authors properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 4 );

		// Parse all web pages
		success = parsePagesWithPCMembers ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Authors properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 5 );

		success = commitPCMembers ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Authors properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 6 );

		success = mergePCMembers ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Authors properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Successfully extracted PC members." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}

	}

	public boolean getAllCFP () {
		boolean success = true;
		builder.text ( "Extracting all CFP from triplestore." );
		PluginInterface.publishPluginStatus ( builder.build () );

		CFPClient cfpClient = CFPClient.create ( APIClient.REST_API );
		Iterator<Cfp> cfps = cfpClient.getAll ();
		int count = 0;
		while ( cfps.hasNext () ) {
			Cfp cfp = cfps.next ();
			if ( cfp.getWebsite () != null && !cfp.getWebsite ().equals ( "" ) ) {
				allCfp.add ( cfp );
			}
			builder.subProgress ( ++count );
			PluginInterface.publishPluginStatus ( builder.build () );
		}
		return success;
	}

	private boolean getAllConferencesWithCFP () {
		boolean success = true;
		builder.text ( "Extracting matching Conferences with CFPs" )
				.subProgress ( 0 )
				.totalSubProgress ( allCfp.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		CFPClient cfpClient = CFPClient.create ( APIClient.REST_API );
		int count = 0;
		for ( Cfp cfp : allCfp ) {
			Conference conference = cfpClient.getConference ( cfp );
			if ( CFPClient.isConferenceValid ( conference ) ) {
				confIdMappingWebsiteRoot.put ( conference.getId (), cfp.getWebsite () );
			}
			builder.subProgress ( ++count );
			PluginInterface.publishPluginStatus ( builder.build () );
		}

		return success;
	}

	private boolean parseRootWebsites () {
		boolean success = true;
		builder.text ( "Extracting Conference root page" )
				.subProgress ( 0 )
				.totalSubProgress ( confIdMappingWebsiteRoot.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		WebPageClient webPageClient = WebPageClient.create ( APIClient.REST_API );
		int count = 0;

		for ( String confId : confIdMappingWebsiteRoot.keySet () ) {
			for ( String url : confIdMappingWebsiteRoot.get ( confId ) ) {
				WebPage webPage = new WebPage ();
				webPage.setUrl ( url );
				webPage = webPageClient.getWebPage ( webPage );
				if ( webPageClient.isValidWebPage ( webPage ) ) {
					Set<String> urls =
							ConferenceUtils.getAllPCMembersPages ( webPage.getContent (), url );
					confIdMappingPCMemberPage.putAll ( confId, urls );
				}
			}

			builder.subProgress ( ++count );
			PluginInterface.publishPluginStatus ( builder.build () );
		}

		return success;
	}

	private boolean parsePagesWithPCMembers () {
		boolean success = true;
		builder.text ( "Extracting Conference root page" )
				.subProgress ( 0 )
				.totalSubProgress ( confIdMappingPCMemberPage.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		WebPageClient webPageClient = WebPageClient.create ( APIClient.REST_API );
		int count = 0;

		for ( String confId : confIdMappingPCMemberPage.keySet () ) {
			for ( String url : confIdMappingPCMemberPage.get ( confId ) ) {
				WebPage webPage = new WebPage ();
				webPage.setUrl ( url );
				webPage = webPageClient.getWebPage ( webPage );
				if ( webPageClient.isValidWebPage ( webPage ) ) {
					Set<String> names =
							ConferenceUtils.getAllPCMembers ( webPage.getContent () );
					for ( String name : names ) {
						pcNameMappingConfId.put ( name, confId );
					}
				}
			}

			builder.subProgress ( ++count );
			PluginInterface.publishPluginStatus ( builder.build () );
		}

		return success;
	}

	private boolean commitPCMembers () {
		boolean success = true;

		builder.text ( "Committing PC Members to triplestore." )
				.subProgress ( 0 )
				.totalSubProgress ( pcNameMappingConfId.keySet ().size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		PeopleClient peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );

		int count = 0;
		for ( String author : pcNameMappingConfId.keySet () ) {
			Person person = new Person ();
			person.setName ( author );
			String personId = peopleClient.post ( person );
			pcIdMappingConfId.putAll ( personId, pcNameMappingConfId.get ( author ) );

			++count;
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
			if ( ( count % 20000 ) == 0 ) {
				session.commitSession ();
				sessionId = session.createSession ();
				peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );
			}
		}
		builder.subProgress ( count );
		PluginInterface.publishPluginStatus ( builder.build () );
		session.commitSession ();

		return success;
	}

	private boolean mergePCMembers () {
		boolean success = true;

		builder.text ( "Merging PC Members and Conferences to triplestore." )
				.subProgress ( 0 )
				.totalSubProgress ( pcIdMappingConfId.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		PeopleClient peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );

		int count = 0;

		for ( String pcId : pcIdMappingConfId.keySet () ) {
			for ( String conferenceKey : pcIdMappingConfId.get ( pcId ) ) {
				peopleClient.addConference ( pcId, conferenceKey );

				++count;
				if ( ( count % 100 ) == 0 ) {
					builder.subProgress ( count );
					PluginInterface.publishPluginStatus ( builder.build () );
				}
				if ( ( count % 20000 ) == 0 ) {
					session.commitSession ();
					sessionId = session.createSession ();
					peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );
				}
			}
		}
		builder.subProgress ( count );
		PluginInterface.publishPluginStatus ( builder.build () );
		session.commitSession ();

		return success;
	}
}
