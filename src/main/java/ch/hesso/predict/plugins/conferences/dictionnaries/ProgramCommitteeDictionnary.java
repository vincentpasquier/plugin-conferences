/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.conferences.dictionnaries;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class ProgramCommitteeDictionnary {

	private static Set<String> _committees = new HashSet<> ();

	static {
		_committees.add ( "committee" );
		_committees.add ( "organisation" );
		_committees.add ( "chair" );
		_committees.add ( "organizer" );
		_committees.add ( "organising" );
		_committees.add ( "administration" );
		_committees.add ( "body" );
		_committees.add ( "commission" );
		_committees.add ( "group" );
		_committees.add ( "panel" );
	}

	public static boolean contains ( final String s ) {
		boolean contains = false;
		String search = s.toLowerCase ();
		for ( String committee : _committees ) {
			contains |= search.contains ( committee );
		}
		return contains;
	}
}
