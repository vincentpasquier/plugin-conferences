/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.conferences;

import ch.hesso.predict.plugins.conferences.dictionnaries.ProgramCommitteeDictionnary;
import com.google.common.base.Joiner;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class ConferenceUtils {

	private ConferenceUtils () {
		throw new IllegalAccessError ( "Utility class, no instance." );
	}


	public static Set<String> getAllPCMembersPages ( final String content, final String url ) {
		Set<String> urls = new HashSet<> ();

		Document doc = Jsoup.parse ( content );
		Elements links = doc.select ( "a[href]" );
		for ( Element link : links ) {
			String text = link.text ().toLowerCase ();
			if ( text.length () > 0 && text.length () < 50 && ProgramCommitteeDictionnary.contains ( text ) ) {
				String href = link.attr ( "abs:href" ).replaceAll ( " ", "%20" );
				urls.add ( href );
			}
		}

		if ( urls.isEmpty () ) {
			Elements headings = doc.select ( "h0, h1, h2, h3, h4, h5, h6" );
			for ( Element heading : headings ) {
				String text = heading.text ().toLowerCase ();
				if ( text.length () > 0 && text.length () < 50 && ProgramCommitteeDictionnary.contains ( text ) ) {
					urls.add ( url );
				}
			}
		}

		return urls;
	}

	public static Set<String> getAllPCMembers ( final String content ) {
		Set<String> names = new HashSet<> ();
		Document doc = Jsoup.parse ( content );
		Element body = doc.body ();
		Elements bullets = body.select ( "li, ol" );
		Elements spans = body.select ( "p" );
		Elements tableRows = body.select ( "tr" );
		Elements divs = body.select ( "div" );
		extract ( names, bullets, spans, tableRows, divs );
		return names;
	}

	@SuppressWarnings ( "unchecked" )
	private static void extract ( final Set<String> pcMembers, final Elements... aElements ) {
		for ( Elements elements : aElements ) {
			for ( Element element : elements ) {
				String text = Jsoup.clean ( element.html (), Whitelist.basic () );
				text = StringEscapeUtils.unescapeHtml ( text );
				text = deAccent ( text );
				text = text.replaceAll ( "(\\(|\\)|,|\\.|;)", "" );
				if ( text.length () > 150 ) {
					continue;
				}
				// Yeah it's done knowingly!
				List<List<CoreLabel>> allLabels =
						ConferencesPluginInterface.CLASSIFIER.classify ( text );
				boolean startPerson = false;
				List<String> founds = new ArrayList<> ();
				for ( List<CoreLabel> labels : allLabels ) {
					for ( CoreLabel label : labels ) {
						if ( label.get ( CoreAnnotations.AnswerAnnotation.class ).equals ( "PERSON" ) ) {
							startPerson = true;
							if ( !label.word ().toLowerCase ().contains ( "univer" ) ) {
								founds.add ( label.word () );
							}
						} else {
							if ( startPerson ) {
								if ( founds.size () > 1 ) {
									String person = Joiner.on ( " " ).join ( founds );
									pcMembers.add ( person );
								}
								startPerson = false;
								founds = new ArrayList<> ();
							}
						}
					}
				}
			}
		}
	}

	public static String deAccent ( String str ) {
		String nfdNormalizedString = Normalizer.normalize ( str, Normalizer.Form.NFD );
		Pattern pattern = Pattern.compile ( "\\p{InCombiningDiacriticalMarks}+" );
		return pattern.matcher ( nfdNormalizedString ).replaceAll ( "" );
	}
}
